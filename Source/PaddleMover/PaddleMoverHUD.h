

#pragma once

#include "GameFramework/HUD.h"
#include "PaddleMoverHUD.generated.h"

/**
 * 
 */
UCLASS()
class APaddleMoverHUD : public AHUD
{
	GENERATED_UCLASS_BODY()


		UFUNCTION()
		virtual void DrawHUD() OVERRIDE;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Breakout)
		UFont* HUDFont;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Breakout)
		FVector2D LivesLabelPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Breakout)
		FLinearColor HUDTextColor;

	class APaddleMoverPlayerState* MyPlayerState;
};
