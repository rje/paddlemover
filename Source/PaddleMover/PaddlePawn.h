

#pragma once

#include "GameFramework/Pawn.h"
#include "PaddleMovementComponent.h"
#include "PaddlePawn.generated.h"
/**
 * 
 */
UCLASS()
class APaddlePawn : public APawn
{
	GENERATED_UCLASS_BODY()


		UPROPERTY()
		TSubobjectPtr<UPaddleMovementComponent> Mover;
};
