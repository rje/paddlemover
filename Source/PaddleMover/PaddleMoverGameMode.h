

#pragma once

#include "GameFramework/GameMode.h"
#include "PaddleMoverGameMode.generated.h"

/**
 * 
 */
UCLASS()
class APaddleMoverGameMode : public AGameMode
{
	GENERATED_UCLASS_BODY()

	virtual void BeginPlay() OVERRIDE;
	
	
};
