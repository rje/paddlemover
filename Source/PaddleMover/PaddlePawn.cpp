

#include "PaddleMover.h"
#include "PaddlePawn.h"


APaddlePawn::APaddlePawn(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	Mover = PCIP.CreateDefaultSubobject<UPaddleMovementComponent>(this, TEXT("Mover"));
}