

#pragma once

#include "GameFramework/Actor.h"
#include "Block.generated.h"

/**
 *
 */
UCLASS()
class ABlock : public AActor
{
	GENERATED_UCLASS_BODY()

		UFUNCTION()
		void OnBlockHit(AActor * SelfActor, AActor * OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Breakout)
		UClass* ToSpawn;
};
