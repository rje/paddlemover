

#include "PaddleMover.h"
#include "PaddleMoverHUD.h"
#include "PaddleMoverPlayerState.h"

APaddleMoverHUD::APaddleMoverHUD(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{

}


void APaddleMoverHUD::DrawHUD()
{
	AHUD::DrawHUD();
	if (MyPlayerState) {
		DrawText(FString::Printf(TEXT("Lives: %d"), MyPlayerState->Lives), HUDTextColor, 10, 10, HUDFont);
	}
}