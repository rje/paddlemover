

#include "PaddleMover.h"
#include "PaddleMovementComponent.h"


UPaddleMovementComponent::UPaddleMovementComponent(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	bWantsInitializeComponent = true;
}

void UPaddleMovementComponent::InitializeComponent() {
	Super::InitializeComponent();
}


void UPaddleMovementComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	if (SkipUpdate(DeltaTime)) {
		return;
	}
	Velocity = Velocity * MovementAxes;
	FHitResult Hit(1.f);
	SafeMoveUpdatedComponent(Velocity * DeltaTime, FRotator(0, 0, 0), true, Hit);
	UpdateComponentVelocity();
}