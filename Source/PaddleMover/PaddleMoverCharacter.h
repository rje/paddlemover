

#pragma once

#include "GameFramework/Character.h"
#include "PaddleMoverCharacter.generated.h"

/**
 * 
 */
UCLASS()
class APaddleMoverCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) OVERRIDE;
	virtual void BeginPlay() OVERRIDE;

	UFUNCTION()
	void Move(float Val);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Breakout)
	APawn* Paddle;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Breakout)
    float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Breakout)
	UCameraComponent* Camera;

	float OriginalY;
	float OriginalZ;
};
