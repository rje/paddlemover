

#pragma once

#include "GameFramework/PawnMovementComponent.h"
#include "PaddleMovementComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup=PaddleMover, meta=(BlueprintSpawnableComponent))
class UPaddleMovementComponent : public UMovementComponent
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Constraints)
	FVector MovementAxes;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Constraints)
	float MaxSpeed;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) OVERRIDE; 
	virtual float GetMaxSpeed() const OVERRIDE { return MaxSpeed; }
	virtual void InitializeComponent() OVERRIDE;
	
};
