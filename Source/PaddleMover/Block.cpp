

#include "PaddleMover.h"
#include "Block.h"


ABlock::ABlock(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	OnActorHit.AddDynamic(this, &ABlock::OnBlockHit);
}

void ABlock::OnBlockHit(AActor * SelfActor, AActor * OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	auto pos = GetActorLocation();
	auto rot = GetActorRotation();
	GetWorld()->SpawnActor(ToSpawn, &pos, &rot);
	Destroy();
}
