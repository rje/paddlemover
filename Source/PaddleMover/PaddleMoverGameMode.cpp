

#include "PaddleMover.h"
#include "PaddleMoverGameMode.h"
#include "PaddleMoverPlayerController.h"
#include "PaddleMoverCharacter.h"
#include "PaddleMoverHUD.h"
#include "PaddleMoverPlayerState.h"

APaddleMoverGameMode::APaddleMoverGameMode(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
	PlayerControllerClass = APaddleMoverPlayerController::StaticClass();
    static ConstructorHelpers::FObjectFinder<UBlueprint> OurCharacter(TEXT("Blueprint'/Game/Blueprints/CustomizedCharacter.CustomizedCharacter'"));
    if(OurCharacter.Object != NULL) {
        DefaultPawnClass = (UClass*)OurCharacter.Object->GeneratedClass;
    }
	static ConstructorHelpers::FObjectFinder<UBlueprint> OurHUD(TEXT("Blueprint'/Game/Blueprints/CustomizedHUD.CustomizedHUD'"));
	if (OurHUD.Object != NULL) {
		HUDClass = (UClass*)OurHUD.Object->GeneratedClass;
	}
	PlayerStateClass = APaddleMoverPlayerState::StaticClass();
}


void APaddleMoverGameMode::BeginPlay() 
{
	Super::BeginPlay();
}