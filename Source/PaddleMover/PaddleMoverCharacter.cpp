

#include "PaddleMover.h"
#include "PaddleMoverCharacter.h"
#include "PaddleMovementComponent.h"


APaddleMoverCharacter::APaddleMoverCharacter(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
}

void APaddleMoverCharacter::BeginPlay() {
	for (TActorIterator<APawn> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, ActorItr->GetName());
		if (ActorItr->GetName().StartsWith(TEXT("Paddle"))) {
			Paddle = *ActorItr;
		}
	}
	if (Paddle) 
	{
		auto pos = Paddle->GetActorLocation();
		OriginalY = pos.Y;
		OriginalZ = pos.Z;
	}

	GSystemResolution.RequestResolutionChange(800, 600, EWindowMode::Type::Windowed);
}

void APaddleMoverCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	InputComponent->BindAxis("Move", this, &APaddleMoverCharacter::Move);
}

void APaddleMoverCharacter::Move(float Val) {
	if (Paddle) {
		auto comp = Paddle->FindComponentByClass<UPaddleMovementComponent>();
		comp->Velocity = FVector(Val * Speed, 0, 0);
	}
}