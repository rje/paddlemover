

#pragma once

#include "GameFramework/PlayerController.h"
#include "PaddleMoverPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class APaddleMoverPlayerController : public APlayerController
{
	GENERATED_UCLASS_BODY()

		UFUNCTION()
		virtual void BeginPlay() OVERRIDE;
};
