

#pragma once

#include "GameFramework/PlayerState.h"
#include "PaddleMoverPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class APaddleMoverPlayerState : public APlayerState
{
	GENERATED_UCLASS_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Breakout)
		uint32 Lives;
	
};
