

#include "PaddleMover.h"
#include "PaddleMoverPlayerController.h"
#include "PaddleMoverPlayerState.h"
#include "PaddleMoverHUD.h"

APaddleMoverPlayerController::APaddleMoverPlayerController(const class FPostConstructInitializeProperties& PCIP)
	: Super(PCIP)
{
}

void APaddleMoverPlayerController::BeginPlay() {
	auto ps = static_cast<APaddleMoverPlayerState*>(PlayerState);
	auto pmHud = static_cast<APaddleMoverHUD*>(GetHUD());
	pmHud->MyPlayerState = ps;
}